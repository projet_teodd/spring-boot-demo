package fr.uvsq.tod;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class Controller {
	
	private ArrayList<Tache> lTaches = new ArrayList<Tache>();
    
	
	@RequestMapping(method = RequestMethod.GET)
	   public ArrayList<Tache> liste()
	   { 
	    return lTaches;
       }
@RequestMapping(method = RequestMethod.POST)
	   public Tache ajoutTache(String title)
	   { 
	    Tache t1 = new Tache(title);
	    lTaches.add(t1);
	    return t1;
       }
}

	   
      

